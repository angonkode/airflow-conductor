FROM apache/airflow:2.1.3-python3.8
RUN pip install --no-cache-dir apache-airflow-providers-microsoft-mssql
